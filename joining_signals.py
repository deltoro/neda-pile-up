import sys
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt

#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)

shift_n = 0
shift_g = 4

data_gamma = pd.DataFrame()
data_neutron = pd.DataFrame()
data_result = pd.DataFrame()
data_zeros = pd.DataFrame()

df_gamma = pd.read_csv ('gamma_signal.csv')
df_neutron = pd.read_csv ('neutron_signal.csv')

def zerolistmaker(n):
       listofzeros = [0] * n
       return listofzeros

zeros_n = zerolistmaker(shift_n)
zeros_g = zerolistmaker(shift_g)

zeros_n = pd.DataFrame({"Neutron_signal": zeros_n})
df_neutron = zeros_n.append(df_neutron.iloc[:, 1:], ignore_index = True) 

zeros_g = pd.DataFrame({"Gamma_signal": zeros_g})
df_gamma = zeros_g.append(df_gamma.iloc[:, 1:], ignore_index = True) 

df = df_neutron.join(df_gamma)
df["Result"]= df.sum(axis=1)  # El "axis=1" para sumar columnas.

data_gamma = df['Gamma_signal']
data_neutron = df['Neutron_signal']
data_result = df['Result']

plt.plot(data_result.index.values,data_gamma,'--',data_result.index.values,data_neutron,'--',data_result.index.values,data_result)
plt.ylabel('values')
plt.show()